const airbnb = require('@neutrinojs/airbnb');
const react = require('@neutrinojs/react');
const jest = require('@neutrinojs/jest');

module.exports = {
  options: {
    root: __dirname
  },
  use: [
    airbnb({
      eslint: {
        useEslintrc: true
      }
    }),
    react({
      html: {
        title: 'Becas CEICH'
      }
    }),
    jest(),
  ],
};
