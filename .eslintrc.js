const neutrino = require('neutrino');

module.exports = {
    root: true,
    extends: 'eslint-config-airbnb',
    plugins: [ 'jest' ],
    env: {
        browser: true,
        node: true,
        'jest/globals': true
    },
    settings: {
        'import/resolver': {
            node: {
                paths: ['src', 'submodules']
            }
        }
    }
};

